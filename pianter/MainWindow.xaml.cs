﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Ninject;
using Paint.Library;
using ShapeDrawing;
using ShapeDrawing.Saving;
using RadioButton = System.Windows.Controls.RadioButton;


namespace painter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
		private ShapeHistory _shapeHistory;
        private DrawStrategy _currentStrategy;
        private Dictionary<RadioButton, DrawStrategy> _radioButtonWithStrategy;
        public static RoutedCommand UndoKeyCommand;
        public static RoutedCommand RedokeyCommand;
        public static RoutedCommand SaveProjectCommand;
        //public string SavedProjectFilePath = String.Empty;
        public IProjectManager ProjectManager;

        public MainWindow()
        {
            InitializeComponent();    

            // Инициализация менеджера проектов
            var kernel =  new StandardKernel(new NinjectConfig());
            ProjectManager = kernel.Get<IProjectManager>();

            // Инициализация истории проекта
            _shapeHistory = new ShapeHistory(canvas);

            // Загрузка плагинов
            LoadDrawStrategy();

            // Инициализация горячих клавиш
            SetHotKeys();         
        }
        
        /// <summary>
        /// Устанавливает связи между командами и горячими клавишами.
        /// </summary>
        private void SetHotKeys()
        {
            UndoKeyCommand = new RoutedCommand();
            RedokeyCommand = new RoutedCommand();
            SaveProjectCommand = new RoutedCommand();

            UndoKeyCommand.InputGestures.Add(new KeyGesture(Key.Z, ModifierKeys.Control));
            RedokeyCommand.InputGestures.Add(new KeyGesture(Key.Y, ModifierKeys.Control));
            SaveProjectCommand.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));

            this.CommandBindings.Add(new CommandBinding(UndoKeyCommand, UndoButton_Click));
            this.CommandBindings.Add(new CommandBinding(RedokeyCommand, RedoButton_Click));
            this.CommandBindings.Add(new CommandBinding(SaveProjectCommand, SaveProjectMenuItem));
        }

        /// <summary>
        /// Добавляет контролы для выбора фигур
        /// </summary>
        private void LoadDrawStrategy()
        {
            var drawStrategyCreator = new DrawStrategyCreator(canvas, GetStrokeColor, GetFillStroke, GetStrokeThickness,
                IsDashed, _shapeHistory);
            var strategies = drawStrategyCreator.GetDrawStrategyFromLibrary();

            _radioButtonWithStrategy = new Dictionary<RadioButton, DrawStrategy>();

            foreach (var strategy in strategies)
            {
                var radionButton = new RadioButton()
                {
                    Content = strategy.ControlName,
                };
                radionButton.Checked += RadioButtonChecked;
                shapeControlsPanel.Children.Add(radionButton);
                _radioButtonWithStrategy.Add(radionButton, strategy);
            }

            if (_radioButtonWithStrategy != null && _radioButtonWithStrategy.Count > 0)
            {
                _radioButtonWithStrategy.Keys.ElementAt(0).IsChecked = true;
            }           
        }

        /// <summary>
        /// Обработчик события выбора радиобаттона.
        /// </summary>
        /// <param name="sender"> Объект, который вызвал событие</param>
        /// <param name="e">Содержит данные для события</param>
        private void RadioButtonChecked(object sender, RoutedEventArgs e)
        {
            _currentStrategy?.Clear();
            var radioButton = (RadioButton) sender;
            _currentStrategy = _radioButtonWithStrategy[radioButton];
            _currentStrategy.Initialize();
        }

        /// <summary>
        /// Обработчик события нажатия левой кнопки мыши для канваса.
        /// </summary>
        /// <param name="sender"> Объект, который вызвал событие</param>
        /// <param name="e">Содержит данные для события</param>
        private void CanvasMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _currentStrategy?.MouseLeftButtonDown();
        }

        /// <summary>
        /// Обработчик события отжатия левой кнопки мыши для канваса.
        /// </summary>
        /// <param name="sender"> Объект, который вызвал событие</param>
        /// <param name="e">Содержит данные для события</param>
        private void CanvasMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _currentStrategy?.MouseLeftButtonUp();
        }

        /// <summary>
        /// Обработчик события нажатия правой кнопки мыши для канваса.
        /// </summary>
        /// <param name="sender"> Объект, который вызвал событие</param>
        /// <param name="e">Содержит данные для события</param>
        private void CanvasMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            _currentStrategy?.MouseRightButtonUp();
        }

        /// <summary>
        /// Обработчик события нажатия кнопки Clear All
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearButtonClick(object sender, RoutedEventArgs e)
        {
            canvas.Children.Clear();
        }

        private void comboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            switch (canvasSizeComboBox.SelectedIndex)
            {
                case 2:
                    canvas.Height = 700;
                    canvas.Width = 300;
                    break;
                case 1:
                    canvas.Height = 1000;
                    canvas.Width = 1000;
                    break;
                case 0:
                    canvas.Height = 1500;
                    canvas.Width = 1500;
                    break;
            }
        }

        private void CanvasMouseMove(object sender, MouseEventArgs e)
        {
            _currentStrategy?.MouseMove(); 
        }

        private Color GetStrokeColor()
        {
            if (strokeColorPicker.SelectedColor != null)
            {
                return (Color) strokeColorPicker.SelectedColor;
            }
            return Colors.Black;
        }

        private Color GetFillStroke()
        {
            if (fillColorPicker.SelectedColor != null)
            {
                return (Color) fillColorPicker.SelectedColor;
            }
            return Colors.Transparent;
        }

        private int GetStrokeThickness()
        {
            return Int16.Parse(thickComboBox.Text);
        }

        private bool IsDashed()
        {
            //return Dash size
            return dashedCheckBox.IsChecked != null && (bool) dashedCheckBox.IsChecked;
        }

        #region Shape properties

        private void StrokeColorPicker_OnSelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> routedPropertyChangedEventArgs)
        {
            _currentStrategy?.SetStrokeColor();
        }

        private void FillColorPicker_OnSelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
             _currentStrategy?.SetFillColor();

        }

        private void DashedCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            _currentStrategy?.SetDashed();
        }

        private void ThickComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            _currentStrategy?.SetStrokeThikness();
        }

		#endregion

		private void UndoButton_Click(object sender, RoutedEventArgs e)
		{
			_shapeHistory.Undo();
		}

		private void RedoButton_Click(object sender, RoutedEventArgs e)
		{
			_shapeHistory.Redo();
		}
      
        /// <summary>
        /// Создать Новый проект
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewFileMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SetHotKeys();

            MessageBoxResult messageBoxResult = MessageBox.Show("Сохранить текущий проект?", "Сохранение проекта",
                MessageBoxButton.YesNoCancel);
            switch (messageBoxResult)
            {
                case MessageBoxResult.Yes:
                    {
                        ProjectManager.SaveProject(canvas, _shapeHistory);
                        var kernel = new StandardKernel(new NinjectConfig());
                        ProjectManager = kernel.Get<IProjectManager>(); //Or clear old one
                        var newShapeHistory = new ShapeHistory(canvas);
                        InitializeNewHistory(newShapeHistory);
                        break;
                    }

                case MessageBoxResult.No:
                    {
                        //ProjectManager = new ProjectManager(); //Or clear old one
                        var kernel = new StandardKernel(new NinjectConfig());
                        ProjectManager = kernel.Get<IProjectManager>();
                        var newShapeHistory = new ShapeHistory(canvas);
                        InitializeNewHistory(newShapeHistory);
                        break;
                    }

                case MessageBoxResult.Cancel:
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Открывает проводник для выбора проекта для открытия
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenFileMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Сохранить проект?", "Сохранение проекта",
                MessageBoxButton.YesNoCancel);
            switch (messageBoxResult)
            {
                case MessageBoxResult.Yes:
                {
                    ProjectManager.SaveProject(canvas, _shapeHistory);

                    //TODO дебаг - проверка на адекватный формат
                    var newShapeHistory = ProjectManager.OpenProject(canvas);
                    InitializeNewHistory(newShapeHistory);
                    break;
                }

                case MessageBoxResult.No:
                {
                    var kernel = new StandardKernel(new NinjectConfig());
                    ProjectManager = kernel.Get<IProjectManager>();
                    //ProjectManager = new ProjectManager(); //Or clear old one
                    var newShapeHistory = ProjectManager.OpenProject(canvas);
                    InitializeNewHistory(newShapeHistory);
                    break;
                }

                case MessageBoxResult.Cancel:
                    break;                
            }

            ProjectManager.GetProjectFilePath();
        }

        private void SaveProjectMenuItem(object sender, RoutedEventArgs e)
        {
            if (ProjectManager.GetProjectFilePath() != String.Empty)
            {
                ProjectManager.SaveProject(canvas, _shapeHistory);
            }
            else
            {
                ProjectManager.SaveProject(canvas, _shapeHistory);
            }            
        }

        private void SaveAsMenuItem(object sender, RoutedEventArgs e)
        {
            var kernel = new StandardKernel(new NinjectConfig());
            ProjectManager = kernel.Get<IProjectManager>();
            //var projectManager = new ProjectManager();
            ProjectManager.SavePictureAs(canvas);
        }

        private void FileCloseMenuItem_Click(object sender, System.ComponentModel.CancelEventArgs e)
        {
            BeforeCloseFile(sender, e);
        }

        private void InitializeNewHistory(ShapeHistory shapeHistory)
        {
            canvas.Children.Clear();
            _shapeHistory.ClearHitsory();
            _shapeHistory = shapeHistory;
            _shapeHistory.Canvas = canvas;
            _shapeHistory.LoadHistory();

            foreach (var strategy in _radioButtonWithStrategy.Values)
            {               
                strategy.Clear();
                strategy.ShapeHistory = shapeHistory;
            }
        }

        private void WindowClosing(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BeforeCloseFile(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Сохранить текущий проект?", "Сохранение проекта",
               MessageBoxButton.YesNoCancel);
            switch (messageBoxResult)
            {
                case MessageBoxResult.Yes:
                    {
                        ProjectManager.SaveProject(canvas, _shapeHistory);
                        break;
                    }

                case MessageBoxResult.No:
                    {
                        break;
                    }

                case MessageBoxResult.Cancel:
                    e.Cancel = true;
                    break;
            }
        }
    }
}
