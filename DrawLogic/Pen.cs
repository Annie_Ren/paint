﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace DrawLogic
{
    public class Pen
    {
        private Canvas _canvas;
        private Shape _shape;
        public Pen(Shape shape, Canvas canvas)
        {
            _shape = shape;
            _canvas = canvas;
        }
        public void DrawShape()
        {
            _canvas.Children.Add(_shape);
        }
    }
}
