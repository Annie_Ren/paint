﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace DrawLogic
{
    public interface ICommand
    {
        void Execute();
        void Redo();
        void Undo();
    }
}
