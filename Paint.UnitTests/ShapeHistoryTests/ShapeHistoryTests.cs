﻿using System.Threading;
using System.Windows.Controls;
using NUnit.Framework;
using Paint.Library;
using TrueFakes;

namespace Paint.UnitTests.ShapeHistoryTests
{
    [TestFixture]
    [Apartment(ApartmentState.STA)]
    public class ShapeHistoryTests
    {
        [Test]
        [TrueFake]
        public void Add_AddCommandInShapeHisotry_IndexIs1()
        {
            // Arrange           
            var canvas = new Canvas();
            var shapeHistory = new ShapeHistory(canvas);
            var command = TrueFake.Of<ICommand>();

            // Act
            shapeHistory.Add(command);

            // Assert
            Assert.AreEqual(1, shapeHistory.CurrentIndex);
        }

        [Test]
        [TrueFake]
        public void Undo_UndoCommandInShapeHistory_IndexIs0()
        {
            // Arrange
            var command = TrueFake.Of<ICommand>();

            var canvas = new Canvas();
            var shapeHistory = new ShapeHistory(canvas);
            shapeHistory.Add(command);

            // Act
            shapeHistory.Undo();

            // Assert
            Assert.AreEqual(0, shapeHistory.CurrentIndex);
        }

        [Test]
        public void Undo_UndoCommandInEmptyShapeHistory_IndexIs0()
        {
            // Arrange
            var canvas = new Canvas();
            var shapeHistory = new ShapeHistory(canvas);

            // Act
            shapeHistory.Undo();

            // Assert
            Assert.AreEqual(0, shapeHistory.CurrentIndex);
        }

        [Test]
        [TrueFake]
        public void Redo_RedoCommandInShapeHistory_IndexIs1()
        {
            // Arrange
            var addCommand = TrueFake.Of<ICommand>();

            var canvas = new Canvas();
            var shapeHistory = new ShapeHistory(canvas);
            shapeHistory.Add(addCommand);
            shapeHistory.Undo();
            shapeHistory.Redo();

            // Act
            shapeHistory.Redo();

            // Assert
            Assert.AreEqual(1, shapeHistory.CurrentIndex);
        }

        [Test]
        public void Redo_RedoCommandInEmptyShapeHistory_IndexIs0()
        {
            // Arrange
            var canvas = new Canvas();
            var shapeHistory = new ShapeHistory(canvas);

            // Act
            shapeHistory.Redo();

            // Assert
            Assert.AreEqual(0, shapeHistory.CurrentIndex);
        }

        // TODO todo todotodotododododooooo
        [Test]
        [TrueFake]
        public void ShapeHistory_ShapeHistory_ShapeHistoryCalledOnce()
        {
            // Arrange
            var canvas = new Canvas();
            var shapeHistory = new ShapeHistory(canvas);
            var command1 = TrueFake.Of<ICommand>();

            shapeHistory.Add(command1);

            // Act
            shapeHistory.Undo();

            //Assert
            Verify.WasCalled(() => command1.Cancel(canvas), Times.Once);
        }
    }
}
