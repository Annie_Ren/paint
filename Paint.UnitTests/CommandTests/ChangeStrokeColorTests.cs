﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using NUnit.Framework;
using Paint.Library.Commands;

namespace Paint.UnitTests
{
    [TestFixture]
    [Apartment(ApartmentState.STA)]
    public class ChangeStrokeColorTests
    {
        #region Execute

        [Test]
        public void Execute_SetBlueColorToStrokeLine_LineWithBlueStrokeColor()
        {
            // Arrange
            var line = new Line();
            line.Stroke = new SolidColorBrush(Colors.White);

            var shapes = new List<Shape>() {line};
            var canvas = new Canvas();
            var changeStrokeColorCommand = new ChangeStrokeColorCommand(shapes, Colors.Blue);

            // Act
            changeStrokeColorCommand.Execute(canvas);

            // Assert
            Assert.AreEqual(((SolidColorBrush) line.Stroke).Color, Colors.Blue);
        }

        [Test]
        public void Execute_SetBlueColorToStrokeEllipse_EllipseWithBlueStrokeColor()
        {
            // Arrange
            var ellipse = new Ellipse();
            ellipse.Stroke = new SolidColorBrush(Colors.White);

            var shapes = new List<Shape>() { ellipse };
            var canvas = new Canvas();
            var changeStrokeColorCommand = new ChangeStrokeColorCommand(shapes, Colors.Blue);

            // Act
            changeStrokeColorCommand.Execute(canvas);

            // Assert
            Assert.AreEqual(((SolidColorBrush)ellipse.Stroke).Color, Colors.Blue);
        }

        [Test]
        public void Execute_SetBlueColorToStrokePolygon_PolygonWithBlueStrokeColor()
        {
            // Arrange
            var polygon = new Polygon();
            polygon.Stroke = new SolidColorBrush(Colors.White);

            var shapes = new List<Shape>() { polygon };
            var canvas = new Canvas();
            var changeStrokeColorCommand = new ChangeStrokeColorCommand(shapes, Colors.Blue);

            // Act
            changeStrokeColorCommand.Execute(canvas);

            // Assert
            Assert.AreEqual(((SolidColorBrush)polygon.Stroke).Color, Colors.Blue);
        }

        [Test]
        public void Execute_SetBlueColorToStrokePolyline_PolylineWithBlueStrokeColor()
        {
            // Arrange
            var polyline = new Polyline();
            polyline.Stroke = new SolidColorBrush(Colors.White);

            var shapes = new List<Shape>() { polyline };
            var canvas = new Canvas();
            var changeStrokeColorCommand = new ChangeStrokeColorCommand(shapes, Colors.Blue);

            // Act
            changeStrokeColorCommand.Execute(canvas);

            // Assert
            Assert.AreEqual(((SolidColorBrush)polyline.Stroke).Color, Colors.Blue);
        }

        #endregion Execute

        #region Cancel

        [Test]
        public void Cancel_ReturnWhiteColorToStrokeLine_LineWithWhiteStrokeColor()
        {
            // Arrange
            var line = new Line();
            line.Stroke = new SolidColorBrush(Colors.White);

            var shapes = new List<Shape>() { line };
            var canvas = new Canvas();
            var changeStrokeColorCommand = new ChangeStrokeColorCommand(shapes, Colors.Blue);
            changeStrokeColorCommand.Execute(canvas);

            // Act
            changeStrokeColorCommand.Cancel(canvas);

            // Assert
            Assert.AreEqual(((SolidColorBrush)line.Stroke).Color, Colors.White);
        }

        #endregion
    }
}
