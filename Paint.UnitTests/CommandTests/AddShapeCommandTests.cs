﻿using System.Threading;
using System.Windows.Controls;
using System.Windows.Shapes;
using NUnit.Framework;
using Paint.Library.Commands;

namespace Paint.UnitTests
{
    [TestFixture]
    [Apartment(ApartmentState.STA)]
    public class AddShapeCommandTests
    {
        #region ExecuteTests
         
        [Test]
        public void Execute_DrawLine_LineInCanvasChildren()
        {
            // Arrange           
            var line = new Line();
            var canvas = new Canvas();

            // Act
            var addShapeCommand = new AddShapeCommand(line);
            addShapeCommand.Execute(canvas);

            // Assert
            Assert.Contains(line, canvas.Children);
        }

        [Test]
        public void Execute_DrawEllipse_EllipseInCanvasChildren()
        {
            // Arrange
            var ellipse = new Ellipse();
            var canvas = new Canvas();

            // Act
            var addShapeCommand = new AddShapeCommand(ellipse);
            addShapeCommand.Execute(canvas);

            // Assert
            Assert.Contains(ellipse, canvas.Children);
        }

        [Test]
        public void Execute_DrawPolyline_PolylineInCanvasChildren()
        {
            // Arrange
            var polyline = new Polyline();
            var canvas = new Canvas();

            // Act
            var addShapeCommand = new AddShapeCommand(polyline);
            addShapeCommand.Execute(canvas);

            // Assert
            Assert.Contains(polyline, canvas.Children);
        }

        [Test]
        public void Execute_DrawPolygon_PolygonInCanvasChildren()
        {
            // Arrange
            var polygon = new Polygon();
            var canvas = new Canvas();

            // Act
            var addShapeCommand = new AddShapeCommand(polygon);
            addShapeCommand.Execute(canvas);

            // Assert
            Assert.Contains(polygon, canvas.Children);
        }

        #endregion

        #region Cancel Tests
        [Test]
        public void Cancel_CanvasIsEmptyAfterAddingLine_CanvasChildrenIsEmpty()
        {
            // Arrange           
            var line = new Line();
            var canvas = new Canvas();
            var addShapeCommand = new AddShapeCommand(line);
            addShapeCommand.Execute(canvas);

            // Act
            addShapeCommand.Cancel(canvas);

            // Assert
            Assert.AreEqual(0, canvas.Children.Count);
        }

        [Test]
        public void Cancel_CanvasIsEmptyAfterAddingEllipse_CanvasChildrenIsEmpty()
        {
            // Arrange           
            var ellipse = new Ellipse();
            var canvas = new Canvas();
            var addShapeCommand = new AddShapeCommand(ellipse);
            addShapeCommand.Execute(canvas);

            // Act
            addShapeCommand.Cancel(canvas);

            // Assert
            Assert.AreEqual(0, canvas.Children.Count);
        }

        [Test]
        public void Cancel_CanvasIsEmptyAfterAddingPolyline_CanvasChildrenIsEmpty()
        {
            // Arrange           
            var polyline = new Polyline();
            var canvas = new Canvas();
            var addShapeCommand = new AddShapeCommand(polyline);
            addShapeCommand.Execute(canvas);

            // Act
            addShapeCommand.Cancel(canvas);

            // Assert
            Assert.AreEqual(0, canvas.Children.Count);
        }

        #endregion
    }
}
