﻿using System.Collections.Generic;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using NUnit.Framework;
using Paint.Library.Commands;

namespace Paint.UnitTests
{
    [TestFixture]
    [Apartment(ApartmentState.STA)]
    public class ChangeFillColorCommandTests
    {
        #region Execute Tests

        [Test]
        public void Execute_SetBlueColorToFillEllipse_BlueFillEllipse()
        {
            // Arrange
            var ellipse = new Ellipse();
            ellipse.Fill = new SolidColorBrush(Colors.White);

            var shapes = new List<Shape>() { ellipse };
            var canvas = new Canvas();
            var changeFillColorCommand = new ChangeFillColorCommand(shapes, Colors.Blue);

            // Act
            changeFillColorCommand.Execute(canvas);

            // Assert
            Assert.AreEqual(((SolidColorBrush)ellipse.Fill).Color, Colors.Blue);
        }

        [Test]
        public void Execute_SetBlueColorToFillPolygon_BlueFillPolygon()
        {
            // Arrange
            var polygon = new Ellipse();
            polygon.Fill = new SolidColorBrush(Colors.White);

            var shapes = new List<Shape>() { polygon };
            var canvas = new Canvas();
            var changeFillColorCommand = new ChangeFillColorCommand(shapes, Colors.Blue);

            // Act
            changeFillColorCommand.Execute(canvas);

            // Assert
            Assert.AreEqual(((SolidColorBrush)polygon.Fill).Color, Colors.Blue);
        }

        #endregion

        #region Cancel Tests

        [Test]
        public void Cancel_ReturnWhiteColorToFillEllipse_WhiteFillEllipse()
        {
            // Arrange
            var ellipse = new Ellipse();
            ellipse.Fill = new SolidColorBrush(Colors.White);

            var shapes = new List<Shape>() { ellipse };
            var canvas = new Canvas();
            var changeFillColorCommand = new ChangeFillColorCommand(shapes, Colors.Blue);
            changeFillColorCommand.Execute(canvas);

            // Act
            changeFillColorCommand.Cancel(canvas);

            // Assert
            Assert.AreEqual(((SolidColorBrush)ellipse.Fill).Color, Colors.White);
        }

        [Test]
        public void Cancel_ReturnWhiteColorToFillPolygon_WhiteFillPolygon()
        {
            // Arrange
            var polygon = new Ellipse();
            polygon.Fill = new SolidColorBrush(Colors.White);

            var shapes = new List<Shape>() { polygon };
            var canvas = new Canvas();
            var changeFillColorCommand = new ChangeFillColorCommand(shapes, Colors.Blue);
            changeFillColorCommand.Execute(canvas);

            // Act
            changeFillColorCommand.Cancel(canvas);

            // Assert
            Assert.AreEqual(((SolidColorBrush)polygon.Fill).Color, Colors.White);
        }
        
        #endregion
    }
}
