﻿using System.Collections.Generic;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Shapes;
using NUnit.Framework;
using Paint.Library.Commands;

namespace Paint.UnitTests.CommandTests
{
    [TestFixture]
    [Apartment(ApartmentState.STA)]
    public class ChangeStrokeThiknessCommandTests
    {
        [Test]
        public void Execute_SetLineStrokeThikness3_LineStrokeThikness3()
        {
            // Assert
            var line = new Line();
            var shapes = new List<Shape> {line};
            var command = new ChangeStrokeThicknessCommand(shapes, 3);
            var canvas = new Canvas();
            // Act
            command.Execute(canvas);
            
            // Assert
            Assert.AreEqual((int) line.StrokeThickness, 3);
        }

        [Test]
        public void Execute_SetEllipseStrokeThikness3_EllipseStrokeThikness3()
        {
            // Assert
            var ellipse = new Ellipse();
            var shapes = new List<Shape> { ellipse };
            var command = new ChangeStrokeThicknessCommand(shapes, 3);
            var canvas = new Canvas();
            // Act
            command.Execute(canvas);

            // Assert
            Assert.AreEqual((int) ellipse.StrokeThickness, 3);
        }

        [Test]
        public void Cancel_ReturnLineStrokeThiknessfrom3to2_LineStrokeThikness2()
        {
            // Assert
            var line = new Line();
            var shapes = new List<Shape> { line };
            line.StrokeThickness = 2;
            var command = new ChangeStrokeThicknessCommand(shapes, 3);
            var canvas = new Canvas();
            command.Execute(canvas);

            // Act
            command.Cancel(canvas);

            // Assert
            Assert.AreEqual((int) line.StrokeThickness, 2);
        }
    }
}
