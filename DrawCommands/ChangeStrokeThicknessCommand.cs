﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace DrawCommands
{
    public class ChangeStrokeThicknessCommand : ICommand
    {
        public double NewStrokeThikness { get; set; }
        private Dictionary<Shape, double> _shapesWithOldThikness = new Dictionary<Shape, double>();
        private Dictionary<int, double> _shapesIndexesWithOldThikness = new Dictionary<int, double>();

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public Dictionary<int, double> ShapesIndexesWithOldThikness
        {
            get { return _shapesIndexesWithOldThikness; }
            set { _shapesIndexesWithOldThikness = value; }
        }

        public ChangeStrokeThicknessCommand() { }

        public ChangeStrokeThicknessCommand(List<Shape> shapes, double newThickness)
        {
            NewStrokeThikness = newThickness;

            foreach (var shape in shapes)
            {
                _shapesWithOldThikness.Add(shape, shape.StrokeThickness);
            }
        }

        public void Execute(Canvas canvas)
        {
            if (_shapesWithOldThikness?.Count > 0)
            {
                foreach (var shapeWithOldThick in _shapesWithOldThikness)
                {
                    shapeWithOldThick.Key.StrokeThickness = NewStrokeThikness;
                    var shapeIndex = canvas.Children.IndexOf(shapeWithOldThick.Key);

                    if (!_shapesIndexesWithOldThikness.ContainsKey(shapeIndex))
                    {
                        _shapesIndexesWithOldThikness.Add(shapeIndex, shapeWithOldThick.Value);
                    }
                }
            }           
        }

        public void Cancel(Canvas canvas)
        {
            if (_shapesWithOldThikness?.Count > 0)
            {
                foreach (var shapeWithOldThikness in _shapesWithOldThikness)
                {
                    shapeWithOldThikness.Key.StrokeThickness = shapeWithOldThikness.Value;
                }
            }
            else
            {
                _shapesWithOldThikness = new Dictionary<Shape, double>();

                foreach (var indexWithOldThick in _shapesIndexesWithOldThikness)
                {
                    var shape = (Shape) canvas.Children[indexWithOldThick.Key];

                    shape.StrokeThickness = indexWithOldThick.Value;
                    _shapesWithOldThikness.Add(shape, indexWithOldThick.Value);
                }
            }          
        }
    }
}