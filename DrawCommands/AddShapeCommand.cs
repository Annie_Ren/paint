﻿using System.Windows.Controls;
using System.Windows.Shapes;

namespace DrawCommands
{
	public class AddShapeCommand : ICommand
    {
        public Shape Shape { get; set; }
	    public int Index { get; set; }

	    public AddShapeCommand() { }

	    public AddShapeCommand(Shape shape)
		{
			Shape = shape;
		}

		public void Execute(Canvas canvas)
		{
            canvas.Children.Add(Shape);
		    Index = canvas.Children.IndexOf(Shape);
		}

		public void Cancel(Canvas canvas)
		{
            canvas.Children.Remove(Shape);
		}
	}
}
