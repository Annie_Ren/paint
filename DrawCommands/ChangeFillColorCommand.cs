﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace DrawCommands
{
    public class ChangeFillColorCommand : ICommand
    {
        private Dictionary<Shape, Color> _shapesWithOldColors = new Dictionary<Shape, Color>();
        private Dictionary<int, Color> _shapesIndexesWithOldColors = new Dictionary<int, Color>();

        public Color NewColor { get; set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public Dictionary<int, Color> ShapesIndexesWithOldColors
        {
            get { return _shapesIndexesWithOldColors; }
            set { _shapesIndexesWithOldColors = value; }
        }

        public ChangeFillColorCommand()
        {
        }

        public ChangeFillColorCommand(List<Shape> shapes, Color newColor)
        {
            foreach (var shape in shapes)
            {
                _shapesWithOldColors.Add(shape, ((SolidColorBrush) shape.Fill).Color);
            }

            NewColor = newColor;
        }

        public void Execute(Canvas canvas)
        {
            if (_shapesWithOldColors?.Count > 0)
            {
                foreach (var shapeWithOldColor in _shapesWithOldColors)
                {
                    shapeWithOldColor.Key.Fill = new SolidColorBrush(NewColor);

                    var shapeIndex = canvas.Children.IndexOf(shapeWithOldColor.Key);
                    if (!_shapesIndexesWithOldColors.ContainsKey(shapeIndex))
                    {
                        _shapesIndexesWithOldColors.Add(shapeIndex, shapeWithOldColor.Value);
                    }
                }
            }
        }

        public void Cancel(Canvas canvas)
        {
            if (_shapesWithOldColors?.Count > 0)
            {
                foreach (var shapeWithColor in _shapesWithOldColors)
                {
                    shapeWithColor.Key.Fill = new SolidColorBrush(shapeWithColor.Value);
                }
            }
            else
            {
                _shapesWithOldColors = new Dictionary<Shape, Color>();

                foreach (var indexWithOldColor in _shapesIndexesWithOldColors)
                {
                    var shape = (Shape) canvas.Children[indexWithOldColor.Key];

                    shape.Fill = new SolidColorBrush(indexWithOldColor.Value);
                    _shapesWithOldColors.Add(shape, indexWithOldColor.Value);
                }
            }
        }
    }
}
