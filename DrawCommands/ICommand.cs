﻿using System.Windows.Controls;

namespace DrawCommands
{
    public interface ICommand
    {
        void Execute(Canvas canvas);
        void Cancel(Canvas canvas);
    }
}
