﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace DrawCommands
{
    public class ChangeStrokeColorCommand : ICommand
    {
        private Dictionary<Shape, Color> _shapesWithOldColors = new Dictionary<Shape, Color>();
        private Dictionary<int, Color> _shapesIndexesWithOldColors = new Dictionary<int, Color>();

        public Color NewColor { get; set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public Dictionary<int, Color> ShapesIndexesWithOldColors
        {
            get { return _shapesIndexesWithOldColors; }
            set { _shapesIndexesWithOldColors = value; }
        }

        public ChangeStrokeColorCommand() { }

        public ChangeStrokeColorCommand(List<Shape> shapes, Color newColor)
        {
            foreach (var shape in shapes)
            {
                _shapesWithOldColors.Add(shape, ((SolidColorBrush) shape.Stroke).Color);
            }

            NewColor = newColor;
        }

        public void Execute(Canvas canvas)
        {
            if (_shapesWithOldColors?.Count > 0)
            {
                foreach (var shapeWithOldColor in _shapesWithOldColors)
                {
                    shapeWithOldColor.Key.Stroke = new SolidColorBrush(NewColor);

                    var shapeIndex = canvas.Children.IndexOf(shapeWithOldColor.Key);
                    if (!_shapesIndexesWithOldColors.ContainsKey(shapeIndex))
                    {
                        _shapesIndexesWithOldColors.Add(shapeIndex, shapeWithOldColor.Value);
                    }
                }
            }
        }

        public void Cancel(Canvas canvas)
        {
            if (_shapesWithOldColors?.Count > 0)
            {
                foreach (var shapeWithColor in _shapesWithOldColors)
                {
                    shapeWithColor.Key.Stroke = new SolidColorBrush(shapeWithColor.Value);
                }
            }
            else
            {
                _shapesWithOldColors = new Dictionary<Shape, Color>();

                foreach (var indexWithOldColor in _shapesIndexesWithOldColors)
                {
                    var shape = (Shape) canvas.Children[indexWithOldColor.Key];

                    shape.Stroke = new SolidColorBrush(indexWithOldColor.Value);
                    _shapesWithOldColors.Add(shape, indexWithOldColor.Value);
                }
            }
        }
    }
}