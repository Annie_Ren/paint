﻿using System.Windows.Controls;
using Ninject.Modules;
using Paint.Library;
using ShapeDrawing.Saving;

namespace ShapeDrawing
{
    public class NinjectConfig : NinjectModule
    {
        public override void Load()
        {
            Bind<ISerializer<ShapeHistory>>().To<XamlSerializer>();
            Bind<IProjectManager>().To<ProjectManager>();
        }
    }
}
