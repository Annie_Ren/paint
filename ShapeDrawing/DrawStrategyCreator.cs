﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Media;
using Paint.Library;

namespace ShapeDrawing
{
    /// <summary>
    /// Загружает dll с описанием фигур.
    /// </summary>
    public class DrawStrategyCreator
    {
        private readonly Canvas _canvas;
        private readonly Func<Color> _getStrokeColor;
        private readonly Func<Color> _getFillStroke;
        private readonly Func<int> _getStrokeThickness;
        private readonly Func<bool> _isDashed;
        private readonly ShapeHistory _shapeHistory;

        /// <summary>
        /// Стратегии рисования фигур.
        /// </summary>
        [ImportMany(typeof(DrawStrategy))]
        public List<DrawStrategy> ShapesStrategy { get; set; }

        /// <summary>
        /// Конструткор.
        /// </summary>
        /// <param name="canvas">Канвас.</param>
        /// <param name="getStrokeColor">Делегат получения цвета границы.</param>
        /// <param name="getFillStroke">Делегат получения цвета заливки.</param>
        /// <param name="getStrokeThickness">Делегат получения толщины границы фигуры.</param>
        /// <param name="isDashed">Получает значение о пунктирности границы фигуры.</param>
        /// <param name="shapeHistory">История проекта.</param>
        public DrawStrategyCreator(Canvas canvas, Func<Color> getStrokeColor, Func<Color> getFillStroke,
            Func<int> getStrokeThickness, Func<bool> isDashed, ShapeHistory shapeHistory)
        {
            _canvas = canvas;
            _getStrokeColor = getStrokeColor;
            _getFillStroke = getFillStroke;
            _getStrokeThickness = getStrokeThickness;
            _isDashed = isDashed;
            _shapeHistory = shapeHistory;
        }

        /// <summary>
        /// Загружает dll из папки.
        /// </summary>
        /// <returns>Возвращает список стратегий из папки.</returns>
        public List<DrawStrategy> GetDrawStrategyFromLibrary()
        {
            var catalog = new DirectoryCatalog("Shapes");
            var container = new CompositionContainer(catalog);

            container.ComposeExportedValue("currentCanvas", _canvas);
            container.ComposeExportedValue<Func<Color>>("strokeColor", _getStrokeColor);
            container.ComposeExportedValue<Func<Color>>("fillColor", _getFillStroke);
            container.ComposeExportedValue<Func<int>>("strokeThikness", _getStrokeThickness);
            container.ComposeExportedValue<Func<bool>>("isDashed", _isDashed);
            container.ComposeExportedValue("shapeHistory", _shapeHistory);

            container.ComposeParts(this);

            return ShapesStrategy;
        }
    }
}
