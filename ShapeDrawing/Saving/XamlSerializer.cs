﻿using System;
using System.IO;
using System.Windows.Markup;
using Paint.Library;

namespace ShapeDrawing.Saving
{
    public class XamlSerializer : ISerializer<ShapeHistory>
    {

        public void Serialize(ShapeHistory shapeHistory, string fullFilePath)
        {
            FileStream fileStream = new FileStream(fullFilePath, FileMode.Create, FileAccess.Write);
            XamlWriter.Save(shapeHistory, fileStream);
            fileStream.Close();               
        }

        public ShapeHistory Deserialize(string fullFilePath)
        {
            try
            {
                var fileStream = new FileStream(fullFilePath, FileMode.Open, FileAccess.Read);
                var shapeHistory = (ShapeHistory) XamlReader.Load(fileStream);
                return shapeHistory;
            }
            catch (Exception)
            {
                //TODO return exception               
               return new ShapeHistory();
            }           
        }
    }
}
