﻿namespace ShapeDrawing.Saving
{
    /// <summary>
    /// Интерфейс сериализации.
    /// </summary>
    /// <typeparam name="T">Сериализуемый объект.</typeparam>
    public interface ISerializer<T> where T : class 
    {
        /// <summary>
        /// Сериализует данные.
        /// </summary>
        /// <param name="data">Данные.</param>
        /// <param name="pathFile">Путь к файлу.</param>
        void Serialize(T data, string pathFile);

        /// <summary>
        /// Десериализует данные.
        /// </summary>
        /// <param name="pathFile">путь к файлу</param>
        /// <returns>Десериализованный объект</returns>
        T Deserialize(string pathFile);
    }
}
