﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using Ninject;
using Paint.Library;

namespace ShapeDrawing.Saving
{
    /// <summary>
    /// Создает менеджер проектов
    /// </summary>
    public class ProjectManager : IProjectManager
    {
        private string _currentProjectFilePath = String.Empty;

        
        public ISerializer<ShapeHistory> Serializer { get; set; }

        [Inject]
        public ProjectManager(ISerializer<ShapeHistory> serializer)
        {
            Serializer = serializer;
        }

        /// <summary>
        /// Сохраняет историю проекта в txt-файл используя XAML-сериализацию.
        /// </summary>
        /// <param name="canvas">Канвас.</param>
        /// <param name="shapeHistory">Исория проекта.</param>

        public void SaveProject(Canvas canvas, ShapeHistory shapeHistory)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text file |*.txt";

            if (saveFileDialog.ShowDialog() == true)
            {
                string fileName = saveFileDialog.FileName;
                _currentProjectFilePath = Path.GetFullPath(fileName);

                Serializer.Serialize(shapeHistory, _currentProjectFilePath);         
            }          
        }

        /// <summary>
        /// Преобразует xaml-содержимое txt-файла в объект ShapeHistory.
        /// </summary>
        /// <param name="canvas"></param>
        /// <returns>Возвращает историю проекта.</returns>
        public ShapeHistory OpenProject(Canvas canvas)
        {
            try
            {
                OpenFileDialog openDialog = new OpenFileDialog();
                openDialog.Filter = "Text file |*.txt";

                if (openDialog.ShowDialog() == true)
                {
                    string fileName = openDialog.FileName;
                    _currentProjectFilePath = Path.GetFullPath(fileName);

                    var kernel = new StandardKernel(new NinjectConfig());
                    var serializer = kernel.Get<ISerializer<ShapeHistory>>();
                    var history = serializer.Deserialize(_currentProjectFilePath);

                    return history;
                }
                else
                {
                    //TODO при закрытии ничего не делать
                    throw new Exception("Какая-то фигня с открытием диалогового окна");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Не удалось загрузить проект. Причина: " + e.Message, "Ошибка открытия проекта",
                    MessageBoxButton.OK, MessageBoxImage.Error);

                return new ShapeHistory();
            }
        }


    #region SaveAsPicture

        /// <summary>
        /// Открывает диалоговое окно для сохранения канвы в качестве изображения
        /// </summary>
        /// <param name="canvas"></param>
        public void SavePictureAs(Canvas canvas)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "PNG File |*.png|JPG File |*.jpg|BMP File |*.bmp";

            if (saveFileDialog.ShowDialog() == true)
            {
                string fileName = saveFileDialog.FileName;
                var extension = Path.GetExtension(saveFileDialog.FileName);
                if (extension != null)
                    switch (extension.ToLower())
                    {
                        case ".jpg":
                        {
                            SaveAsJpg(canvas, fileName);
                            break;
                        }

                        case ".png":
                        {
                            SaveAsPng(canvas, fileName);
                            break;
                        }
                        case ".bmp":
                        {
                            SaveAsBitmap(canvas, fileName);
                            break;
                        }
                        default:
                            throw new ArgumentOutOfRangeException(extension);
                    }
            }
       }

        /// <summary>
        /// Сохраняет канву в формате jpeg
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="fileName"></param>
        private void SaveAsJpg(Canvas canvas, string fileName)
        {
            RenderTargetBitmap jpg = new RenderTargetBitmap(
              (int)canvas.ActualWidth, (int)canvas.ActualHeight, 96, 96, PixelFormats.Pbgra32);

            jpg.Render(canvas);
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(jpg));

            using (Stream stream = new FileStream(fileName, FileMode.Create))
            {
                encoder.Save(stream);
            }
        }

        /// <summary>
        /// Сохраняет канвас в формате bitmap
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="fileName"></param>
        private void SaveAsBitmap(Canvas canvas, string fileName)
        {
            RenderTargetBitmap bmp = new RenderTargetBitmap(
               (int)canvas.ActualWidth, (int)canvas.ActualHeight, 96, 96, PixelFormats.Pbgra32);

            bmp.Render(canvas);
            BmpBitmapEncoder encoder = new BmpBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bmp));

            using (Stream stream = new FileStream(fileName, FileMode.Create))
            {
                encoder.Save(stream);
            }
        }

        /// <summary>
        /// Сохраняет канву в формате png
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="fileName"></param>
        private void SaveAsPng(Canvas canvas, string fileName)
        {
            RenderTargetBitmap png = new RenderTargetBitmap(
             (int)canvas.ActualWidth, (int)canvas.ActualHeight, 96, 96, PixelFormats.Pbgra32);

            png.Render(canvas);
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(png));

            using (Stream stream = new FileStream(fileName, FileMode.Create))
            {
                encoder.Save(stream);
            }
        }

        #endregion

        /// <summary>
        /// Возвращает путь к текущему файлу
        /// </summary>
        /// <returns>Полный путь к файлу</returns>
        public String GetProjectFilePath()
        {
            return _currentProjectFilePath;
        }
    }
}
