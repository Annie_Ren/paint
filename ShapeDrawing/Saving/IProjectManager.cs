﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Paint.Library;

namespace ShapeDrawing.Saving
{
    /// <summary>
    /// Интерфейс управления проектами
    /// </summary>
    public interface IProjectManager
    {
        /// <summary>
        /// Схраняет проект в файл.
        /// </summary>
        /// <param name="canvas">Канвас.</param>
        /// <param name="shapeHistory">История проекта.</param>
        void SaveProject(Canvas canvas, ShapeHistory shapeHistory);

        /// <summary>
        /// Открывает проект из файла.
        /// </summary>
        /// <param name="canvas">Канвас</param>
        /// <returns>Возвращает историю проекта.</returns>
        ShapeHistory OpenProject(Canvas canvas);

        /// <summary>
        /// Сохраняет канвас в графическом представлении.
        /// </summary>
        /// <param name="canvas">Канвас</param>
        void SavePictureAs(Canvas canvas);

        /// <summary>
        /// Получает путь к текущему проекту.
        /// </summary>
        /// <returns>Путь к файлу в строковом представлении.</returns>
        String GetProjectFilePath();
    }
}
