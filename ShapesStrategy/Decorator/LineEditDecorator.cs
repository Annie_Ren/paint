﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Shapes;
using Paint.Library;
using ShapeDrawing;

namespace ShapesStrategy.Decorator
{
    public class LineEditDecorator : IEditDecorator
    {
        public void SetEditAnchors(Shape shape)
        {
            if (shape.GetType() != ShapeType())
            {
                throw new ArgumentException();
            }
            
            var line = shape as Line;

            var thumbs = new Dictionary<Point, Action<object, DragCompletedEventArgs>>()
            {
                {
                    new Point(line.X1 - 5 , line.Y1 - 5), delegate(object o, DragCompletedEventArgs args)
                    {
                        line.X1 = line.X1 + args.HorizontalChange;
                        line.Y1 = line.Y1 + args.VerticalChange;
                    }
                },

                {
                    new Point(line.X2 - 5, line.Y2 - 5), delegate(object o, DragCompletedEventArgs args)
                    {
                        line.X2 = line.X2 + args.HorizontalChange;
                        line.Y2 = line.Y2 + args.VerticalChange;
                    }
                }
            };

            var adornerLayer = AdornerLayer.GetAdornerLayer(line);
            var anchorAdorner = new AnchorAdorner(line, thumbs);
            adornerLayer.Add(anchorAdorner);
            adornerLayer.UpdateLayout();
        }

        public void RemoveEditAnchors(Shape shape)
        {
            var adornerLayer = AdornerLayer.GetAdornerLayer(shape);
            if (adornerLayer != null)
            {
                var anchorAdorners = adornerLayer.GetAdorners(shape);
                if (anchorAdorners != null)
                    foreach (var adorner in anchorAdorners)
                    {
                        adornerLayer.Remove(adorner);
                    }
                adornerLayer.UpdateLayout();
            }            
        }

        public Type ShapeType()
        {
            return typeof(Line);
        }
    }
}
