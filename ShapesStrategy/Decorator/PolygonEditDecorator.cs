﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Shapes;
using Paint.Library;
using ShapeDrawing;

namespace ShapesStrategy.Decorator
{
    public class PolygonEditDecorator : IEditDecorator
    {
        public void SetEditAnchors(Shape shape)
        {
            if (shape.GetType() != ShapeType())
            {
                throw new ArgumentException();
            }

            var polygon = shape as Polygon;
            var thumbs = new Dictionary<Point, Action<object, DragCompletedEventArgs>>();

            for (int i = 0; i < polygon.Points.Count; i++)
            {
                var j = i;
                thumbs.Add(new Point(polygon.Points[i].X - 5, polygon.Points[i].Y - 5), delegate (object o, DragCompletedEventArgs args)
                {
                    polygon.Points[j] += new Vector(args.HorizontalChange, args.VerticalChange);
                }
                );
            }

            var adornerLayer = AdornerLayer.GetAdornerLayer(polygon);
            var anchorAdorner = new AnchorAdorner(polygon, thumbs);
            adornerLayer.Add(anchorAdorner);
            adornerLayer.UpdateLayout();
        }

        public void RemoveEditAnchors(Shape shape)
        {
            var adornerLayer = AdornerLayer.GetAdornerLayer(shape);
            var anchorAdorners = adornerLayer.GetAdorners(shape);
            if (anchorAdorners != null)
                foreach (var adorner in anchorAdorners)
                {
                    adornerLayer.Remove(adorner);
                }
            adornerLayer.UpdateLayout();
        }

        public Type ShapeType()
        {
            return typeof(Polygon);
        }
    }
}
