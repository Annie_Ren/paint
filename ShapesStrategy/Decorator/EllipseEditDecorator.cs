﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Shapes;
using Paint.Library;
using ShapeDrawing;

namespace ShapesStrategy.Decorator
{
    public class EllipseEditDecorator : IEditDecorator
    {
        public void SetEditAnchors(Shape shape)
        {
            if (shape.GetType() != ShapeType())
            {
                throw new ArgumentException();
            }

            var circle = shape as Ellipse;

            var thumbs = new Dictionary<Point, Action<object, DragCompletedEventArgs>>()
            {
                {
                    new Point(circle.Width - 5, circle.Height / 2 - 5), delegate(object o, DragCompletedEventArgs args)
                    {
                        circle.Width = Math.Abs(circle.Width + args.HorizontalChange);
                    }
                },
                {
                    new Point(circle.Width / 2 - 5, circle.Height - 5), delegate(object o, DragCompletedEventArgs args)
                    {
                        circle.Height = Math.Abs(circle.Height + args.VerticalChange);
                    }
                }
            };

            var adornerLayer = AdornerLayer.GetAdornerLayer(circle);
            var anchorAdorner = new AnchorAdorner(circle, thumbs);
            adornerLayer.Add(anchorAdorner);
            adornerLayer.UpdateLayout();
        }

        public void RemoveEditAnchors(Shape shape)
        {
            var adornerLayer = AdornerLayer.GetAdornerLayer(shape);
            var anchorAdorners = adornerLayer.GetAdorners(shape);
            if (anchorAdorners != null)
            {
                foreach (var adorner in anchorAdorners)
                {
                    adornerLayer.Remove(adorner);
                }
            }
                
            adornerLayer.UpdateLayout();
        }

        public Type ShapeType()
        {
            return typeof(Ellipse);
        }
    }
}
