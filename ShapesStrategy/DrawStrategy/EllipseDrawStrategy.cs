﻿using System;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Paint.Library;
using Paint.Library.Commands;

namespace ShapesStrategy.DrawStrategy
{
    /// <summary>
    /// Семейство алгоритмов для построения(рисования) эллипса
    /// </summary>
    [Export(typeof(Paint.Library.DrawStrategy))]
    public class EllipseDrawStrategy : Paint.Library.DrawStrategy
    {
        private Ellipse _ellipse;
        private Ellipse _shadowEllipse;
        private Point _startPoint;

        [ImportingConstructor]
        public EllipseDrawStrategy(
            [Import("currentCanvas")] Canvas currentCanvas,
            [Import("strokeColor")] Func<Color> strokeColor,
            [Import("fillColor")] Func<Color> fillColor,
            [Import("strokeThikness")] Func<int> strokeThikness,
            [Import("isDashed")] Func<bool> isDashed,
            [Import("shapeHistory")] ShapeHistory shapeHistory)
            : base(currentCanvas, strokeColor, fillColor, strokeThikness, isDashed, shapeHistory)
        {
            ControlName = "Ellipse";
        }

        /// <summary>
        /// Обработчик события нажатия левой кнопки мыши
        /// </summary>
        public override void MouseLeftButtonDown()
        {
            CurrentCanvas.CaptureMouse();
            _startPoint = Mouse.GetPosition(CurrentCanvas);
            _shadowEllipse = new Ellipse()
            {
                Stroke = new SolidColorBrush(StrokeColor.Invoke()),
                Fill = new SolidColorBrush(FillColor.Invoke()),
                StrokeThickness = StrokeThikness.Invoke()
            };

            if (IsDashed.Invoke())
            {
                _shadowEllipse.StrokeDashArray = new DoubleCollection(new double[] { StrokeDashSize });
            }
            CurrentCanvas.Children.Add(_shadowEllipse);
        }

        public override void MouseMove()
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed && _shadowEllipse != null)
            {
                Point endPoint = Mouse.GetPosition(CurrentCanvas);
                double deltaX = Math.Abs(_startPoint.X - endPoint.X);
                double deltaY = Math.Abs(_startPoint.Y - endPoint.Y);
                _shadowEllipse.Height = deltaY * 2;
                _shadowEllipse.Width = deltaX * 2;

                Canvas.SetTop(_shadowEllipse, _startPoint.Y - deltaY);
                Canvas.SetLeft(_shadowEllipse, _startPoint.X - deltaX);
            }
        }

        /// <summary>
        /// Обработчик события отжатия левой кнопки мыши
        /// </summary>
        public override void MouseLeftButtonUp()
        {
            _ellipse = _shadowEllipse;
            CurrentCanvas.Children.Remove(_shadowEllipse);
            _shadowEllipse = null;

            var command = new AddShapeCommand(_ellipse);
            ShapeHistory.Add(command);
            CurrentCanvas.ReleaseMouseCapture();
        }
    }
}
