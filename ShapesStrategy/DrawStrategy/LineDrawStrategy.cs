﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Paint.Library;
using Paint.Library.Commands;

namespace ShapesStrategy.DrawStrategy
{
    /// <summary>
    /// Семейство алгоритмов для построения(рисования) линии
    /// </summary>
    [Export(typeof(Paint.Library.DrawStrategy))]
    public class LineDrawStrategy : Paint.Library.DrawStrategy
    {
        private Line _shadowLine;
        private Line _line;

        [ImportingConstructor]
        public LineDrawStrategy([Import("currentCanvas")] Canvas currentCanvas,
            [Import("strokeColor")] Func<Color> strokeColor,
            [Import("fillColor")] Func<Color> fillColor,
            [Import("strokeThikness")] Func<int> strokeThikness,
            [Import("isDashed")] Func<bool> isDashed,
            [Import("shapeHistory")] ShapeHistory shapeHistory)
            : base(currentCanvas, strokeColor, fillColor, strokeThikness, isDashed, shapeHistory)
        {
            ControlName = "Line";
        }

        /// <summary>
        /// Обработчик события нажатия левой кнопки мыши
        /// </summary>
        public override void MouseLeftButtonDown()
        {
            CurrentCanvas.CaptureMouse();
            var startPoint = Mouse.GetPosition(CurrentCanvas);

            _shadowLine = new Line()
            {
                StrokeThickness = StrokeThikness.Invoke(),
                Stroke = new SolidColorBrush(StrokeColor.Invoke()),
                X1 = startPoint.X,
                Y1 = startPoint.Y,
                X2 = startPoint.X,
                Y2 = startPoint.Y
            };
            
            if (IsDashed.Invoke())
            {
                _shadowLine.StrokeDashArray = new DoubleCollection(new double[] { StrokeDashSize });
            }

            CurrentCanvas.Children.Add(_shadowLine);
            Canvas.SetTop(_shadowLine, 0);
            Canvas.SetLeft(_shadowLine, 0);
        }

        public override void MouseMove()
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                var endPoint = Mouse.GetPosition(CurrentCanvas);
                if (_shadowLine != null)
                {
                    _shadowLine.X2 = endPoint.X;
                    _shadowLine.Y2 = endPoint.Y;
                }                
            }
        }

        /// <summary>
        /// Обработчик события отжатия левой кнопки мыши
        /// </summary>
        public override void MouseLeftButtonUp()
        {
            _line = _shadowLine;
            CurrentCanvas.Children.Remove(_shadowLine);
            _shadowLine = null;
			var command = new AddShapeCommand(_line);

			ShapeHistory.Add(command);

            _line = null;
            CurrentCanvas.ReleaseMouseCapture();
        }
    }
}
