﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml;
using Paint.Library;
using Paint.Library.Commands;
using ShapeDrawing;
using ShapesStrategy.Decorator;

namespace ShapesStrategy.DrawStrategy
{
    /// <summary>
    /// Предоставляет обработчики событий мыши для выделения и перемещения фигур
    /// </summary>
    [Export(typeof(Paint.Library.DrawStrategy))]
    public class EditStrategy : Paint.Library.DrawStrategy
    {
        private bool _isDragging;
        private bool _isSelecting;
        private Point _startPoint;       
        private readonly List<DependencyObject> _hitTestResults = new List<DependencyObject>();
        private Shape _selectingArea;
        private Dictionary<Shape, Tuple<double, double>> _selectedShapesWithLeftAndTop = new Dictionary<Shape, Tuple<double, double>>();
        private readonly Dictionary<Type, IEditDecorator> _shapeDecorators;
        private Dictionary<Shape, Tuple<double, double>> _buffer = new Dictionary<Shape, Tuple<double, double>>();

        [ImportingConstructor]
        public EditStrategy(
            [Import("currentCanvas")] Canvas currentCanvas,
            [Import("strokeColor")] Func<Color> strokeColor,
            [Import("fillColor")] Func<Color> fillColor,
            [Import("strokeThikness")] Func<int> strokeThikness,
            [Import("isDashed")] Func<bool> isDashed,
            [Import("shapeHistory")] ShapeHistory shapeHistory) : 
			base(currentCanvas, strokeColor, fillColor, strokeThikness, isDashed, shapeHistory)
        {
            _shapeDecorators = new Dictionary<Type, IEditDecorator>()
                {
                    { typeof(Line), new LineEditDecorator() },
                    { typeof(Ellipse), new EllipseEditDecorator() },
                    { typeof(Polyline), new PolylineEditDecorator() },
                    { typeof(Polygon), new PolygonEditDecorator() }
                };

            ControlName = "Edit";
        }
       
        public override void MouseLeftButtonDown()
        {
            _startPoint = Mouse.GetPosition(CurrentCanvas);
			
            if (_selectingArea != null && _selectingArea.IsMouseOver)
            {
                //Перемещение
                _isDragging = true;
                _isSelecting = false;

                var shapes = new List<Shape>(_selectedShapesWithLeftAndTop.Keys);
                foreach (var shape in shapes)
                {
                    _selectedShapesWithLeftAndTop[shape] = 
                        new Tuple<double, double>(Canvas.GetLeft(shape), Canvas.GetTop(shape));
                }
            }
            else
            {
                //Выделение с удалением старой области выделения (квадрат)
                _isDragging = false;
                _isSelecting = true;
                   
                foreach (var selectedShape in _selectedShapesWithLeftAndTop)
                {
	                if (selectedShape.Key != _selectingArea)
	                {
						var shape = selectedShape.Key;
						_shapeDecorators[shape.GetType()].RemoveEditAnchors(shape);
					}                                                   
                }
                
	            if (_selectingArea != null)
	            {
					CurrentCanvas.Children.Remove(_selectingArea);
					_selectingArea = null;
				}

                CurrentCanvas.CaptureMouse();
                _selectingArea = GetSelectingShape();
	            CurrentCanvas.Children.Add(_selectingArea);
				Canvas.SetTop(_selectingArea, _startPoint.Y);
				Canvas.SetLeft(_selectingArea, _startPoint.X);
			}
        }

        /// <summary>
        /// Обработчик события передвижения мыши
        /// </summary>
        public override void MouseMove()
        {
            if (_isDragging)
            {
                var endPoint = Mouse.GetPosition(CurrentCanvas);

                // Перемещение всех выделенных фигур
                foreach (var shape in _selectedShapesWithLeftAndTop)
                {
                    Canvas.SetLeft(shape.Key, shape.Value.Item1 + (endPoint.X - _startPoint.X));
                    Canvas.SetTop(shape.Key, shape.Value.Item2 + (endPoint.Y - _startPoint.Y));
                }
            }

            if (_isSelecting)
            {
                if (CurrentCanvas.IsMouseCaptured && Mouse.LeftButton == MouseButtonState.Pressed
                    && _selectingArea != null)
                {
                    //Изменение области выделения
                    var x = Math.Min(Mouse.GetPosition(CurrentCanvas).X, _startPoint.X);
                    var y = Math.Min(Mouse.GetPosition(CurrentCanvas).Y, _startPoint.Y);

                    var width = Math.Max(Mouse.GetPosition(CurrentCanvas).X, _startPoint.X) - x;
                    var height = Math.Max(Mouse.GetPosition(CurrentCanvas).Y, _startPoint.Y) - y;

					_selectingArea.Height = height;
					_selectingArea.Width = width;

                    Canvas.SetLeft(_selectingArea, x);
                    Canvas.SetTop(_selectingArea, y);                   
                }
            }
        }

        public override void MouseLeftButtonUp()
        {
            if (_isSelecting)
            {
                // Поиск фигур в выделенной области 
                _hitTestResults.Clear();

                RectangleGeometry hitArea = new RectangleGeometry(
                    new Rect(new Point(Canvas.GetLeft(_selectingArea), Canvas.GetTop(_selectingArea)), 
                        new Size(_selectingArea.Width, _selectingArea.Height)));

                VisualTreeHelper.HitTest(CurrentCanvas, null, HitTestResultCallback,
                    new GeometryHitTestParameters(hitArea));

				_selectedShapesWithLeftAndTop.Clear();

				foreach (var item in _hitTestResults)
                {
	                var shape = item as Shape;
	                if (shape != null)
                    {
						_selectedShapesWithLeftAndTop.Add(shape, null);
                    }
                }

                //Отрисовка опорных узловых точек фигур
                foreach (var selectedShape in _selectedShapesWithLeftAndTop)
                {
	                if (selectedShape.Key != _selectingArea)
	                {
						var shape = selectedShape.Key;
						_shapeDecorators[shape.GetType()].SetEditAnchors(shape);   
	                }                  
                }
            }

            Mouse.Capture(null);
            _isDragging = false;
        }

        /// <summary>
        /// Открывает контекстное меню при нажатии правой кнопки мыши
        /// </summary>
        public override void MouseRightButtonDown()
        {
            CurrentCanvas.ContextMenu.IsOpen = true;
        }

        /// <summary>
        /// Создает и устанавливает параметры области выделения
        /// </summary>
        /// <returns></returns>
        private Rectangle GetSelectingShape()
        {
            var selectingShape = new Rectangle()
            {
                StrokeThickness = 2,
                Stroke = new SolidColorBrush(Colors.DodgerBlue),
                Opacity = 0.3,
                Fill = new SolidColorBrush(Colors.DodgerBlue)
            };

            return selectingShape;
        }

        public HitTestResultBehavior HitTestResultCallback(HitTestResult result)
        {
            _hitTestResults.Add(result.VisualHit);
            return HitTestResultBehavior.Continue;        
        }

        public override void Initialize()
        {
            var contextMenu = new ContextMenu();
            var cutItem = new MenuItem
            {
                Header = "Cut",
                Command = ApplicationCommands.Cut
            };
            contextMenu.Items.Add(cutItem);

            var copyItem = new MenuItem
            {
                Header = "Copy",
                Command = ApplicationCommands.Copy
            };
            contextMenu.Items.Add(copyItem);

            var pasteItem = new MenuItem
            {
                Header = "Paste",
                Command = ApplicationCommands.Paste
            };
            contextMenu.Items.Add(pasteItem);

            CurrentCanvas.ContextMenu = contextMenu;

            CommandBinding cutCmdBinding = new CommandBinding(ApplicationCommands.Cut, CutExecute, CanExecute);
            CommandBinding copyCmdBinding = new CommandBinding(ApplicationCommands.Copy, CopyExecute, CanExecute);
            CommandBinding pasteCmdBinding = new CommandBinding(ApplicationCommands.Paste, PasteExecute, CanExecute);

            ((Grid) ((ScrollViewer)CurrentCanvas.Parent).Parent).CommandBindings.Add(cutCmdBinding);
            ((Grid) ((ScrollViewer)CurrentCanvas.Parent).Parent).CommandBindings.Add(copyCmdBinding);
            ((Grid) ((ScrollViewer)CurrentCanvas.Parent).Parent).CommandBindings.Add(pasteCmdBinding);
        }

        #region ContextMenuCommands

        private void CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CutExecute(object sender, ExecutedRoutedEventArgs e)
        {
            _buffer.Clear();

            foreach (var shape in _selectedShapesWithLeftAndTop)
            {
	            if (shape.Key != _selectingArea)
	            {
					_buffer.Add(shape.Key, new Tuple<double, double>(Canvas.GetLeft(shape.Key), Canvas.GetTop(shape.Key)));
					CurrentCanvas.Children.Remove(shape.Key);
				}                 
            }

            _selectedShapesWithLeftAndTop.Clear();

            if (_selectingArea != null)
            {
                CurrentCanvas.Children.Remove(_selectingArea);
                _selectingArea = null;
            }
        }

        private void CopyExecute(object sender, ExecutedRoutedEventArgs e)
        {
            _buffer.Clear();

            foreach (var shape in _selectedShapesWithLeftAndTop)
            {
                if (shape.Key != _selectingArea)
                {
                    var copyShape = CopyShape(shape.Key);
                    _buffer.Add(copyShape, new Tuple<double, double>(Canvas.GetLeft(shape.Key), Canvas.GetTop(shape.Key)));
                }
            }
        }

        private void PasteExecute(object sender, ExecutedRoutedEventArgs e)
        {
            foreach (var selectedShape in _selectedShapesWithLeftAndTop)
            {
	            if (selectedShape.Key != _selectingArea)
	            {
					var shape = selectedShape.Key;
					_shapeDecorators[shape.GetType()].RemoveEditAnchors(shape);
				}                          
            }

            var cursorPoint = Mouse.GetPosition(CurrentCanvas);
            foreach (var shape in _buffer)
            {
                var copyShape = CopyShape(shape.Key);
                CurrentCanvas.Children.Add(copyShape);

                double deltaX = cursorPoint.X - Canvas.GetLeft(copyShape);
                double deltaY = cursorPoint.Y - Canvas.GetTop(copyShape);

                Canvas.SetLeft(copyShape, shape.Value.Item1 + deltaX);
                Canvas.SetTop(copyShape, shape.Value.Item2 + deltaY);
            }

            CurrentCanvas.Children.Remove(_selectingArea);
            _selectingArea = null;
            _buffer.Clear(); 
        }

        private Shape CopyShape(Shape shape)
        {
            string saved = XamlWriter.Save(shape);
            Shape copyShape = (Shape)XamlReader.Load(XmlReader.Create(new StringReader(saved)));
            Canvas.SetLeft(copyShape, Canvas.GetLeft(shape));
            Canvas.SetTop(copyShape, Canvas.GetTop(shape));

            return copyShape;
        }

        #endregion

        #region Shape property setters 

        public override void SetStrokeColor()
        {
            List<Shape> changingShapes = _selectedShapesWithLeftAndTop.Keys.Where(shape => shape != _selectingArea).ToList();
            if (changingShapes.Count != 0)
            {
                var command = new ChangeStrokeColorCommand(changingShapes, StrokeColor.Invoke());
                ShapeHistory.Add(command);
            }           
        }

        public override void SetFillColor()
        {
            List<Shape> changingShapes = _selectedShapesWithLeftAndTop.Keys.Where(shape => shape != _selectingArea).ToList();
            if (changingShapes.Count != 0)
            {
                var command = new ChangeFillColorCommand(changingShapes, FillColor.Invoke());
                ShapeHistory.Add(command);
            }
        }

        public override void SetDashed()
        {
            List<Shape> shapes = _selectedShapesWithLeftAndTop.Keys.Where(shape => shape !=_selectingArea).ToList();
            if (shapes.Count != 0)
            {
                var command = new SetDashedCommand(shapes, IsDashed.Invoke(), StrokeDashSize);
                ShapeHistory.Add(command);
            }
        }

        public override void SetStrokeThikness()
        {
            List<Shape> shapes = _selectedShapesWithLeftAndTop.Keys.Where(shape => shape != _selectingArea).ToList();
            if (shapes.Count != 0)
            {
                var command = new ChangeStrokeThicknessCommand(shapes, StrokeThikness.Invoke());
                ShapeHistory.Add(command);
            }
        }

    #endregion

        public override void Clear()
        {
            if (_selectingArea != null)
            {
                CurrentCanvas.Children.Remove(_selectingArea);
                _selectingArea = null;
            }
            
            _selectedShapesWithLeftAndTop.Clear();
            _buffer.Clear();
	        CurrentCanvas.ContextMenu = null;

        }
    }
}

