﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Paint.Library;
using Paint.Library.Commands;

namespace ShapesStrategy.DrawStrategy
{
    /// <summary>
    /// Семейство алгоритмов для построения(рисования) полигона (многоугольника)
    /// </summary>
    [Export(typeof(Paint.Library.DrawStrategy))]
    public class PolygonDrawStrategy : Paint.Library.DrawStrategy
    {
        private Polygon _polygon;

        [ImportingConstructor]
        public PolygonDrawStrategy(
            [Import("currentCanvas")] Canvas currentCanvas,
            [Import("strokeColor")] Func<Color> strokeColor,
            [Import("fillColor")] Func<Color> fillColor,
            [Import("strokeThikness")] Func<int> strokeThikness,
            [Import("isDashed")] Func<bool> isDashed,
            [Import("shapeHistory")] ShapeHistory shapeHistory)
            : base(currentCanvas, strokeColor, fillColor, strokeThikness, isDashed, shapeHistory)
        {
            ControlName = "Polygon";
        }

        /// <summary>
        /// Обработчик события нажатия левой кнопки мыши
        /// </summary>
        public override void MouseLeftButtonDown()
        {
            if (_polygon == null)
            {
                _polygon = new Polygon()
                {
                    Stroke = new SolidColorBrush(StrokeColor.Invoke()),
                    Fill = new SolidColorBrush(FillColor.Invoke()),
                    StrokeThickness = StrokeThikness.Invoke(),
                    Points = new PointCollection()
                };
                if (IsDashed.Invoke())
                {
                    _polygon.StrokeDashArray = new DoubleCollection(new double[] { StrokeDashSize });
                }
            }

            double x = Mouse.GetPosition(CurrentCanvas).X;
            double y = Mouse.GetPosition(CurrentCanvas).Y;

            _polygon.Points.Add(new System.Windows.Point(Convert.ToInt32(x), Convert.ToInt32(y)));
        }

        /// <summary>
        /// Обработчик события отжатия правой кнопки мыши
        /// </summary>
        public override void MouseRightButtonUp()
        {
            if (_polygon == null) return;
            var command = new AddShapeCommand(_polygon);
            ShapeHistory.Add(command);

            Canvas.SetLeft(_polygon, 0);
            Canvas.SetTop(_polygon, 0);
            _polygon = null;
        }
    }
}