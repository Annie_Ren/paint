﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Paint.Library;
using Paint.Library.Commands;
using Color = System.Windows.Media.Color;
using Point = System.Windows.Point;

namespace ShapesStrategy.DrawStrategy
{
    /// <summary>
    /// Предоставляет 
    /// </summary>
    [Export(typeof(Paint.Library.DrawStrategy))]
    public class CircleDrawStrategy : Paint.Library.DrawStrategy
    {
        private Ellipse _circle;
        private Ellipse _shadowCircle;
        private Point _startPoint;

        [ImportingConstructor]
        public CircleDrawStrategy(
            [Import("currentCanvas")] Canvas currentCanvas,
            [Import("strokeColor")] Func<Color> strokeColor,
            [Import("fillColor")] Func<Color> fillColor,
            [Import("strokeThikness")] Func<int> strokeThikness,
            [Import("isDashed")] Func<bool> isDashed, 
            [Import("shapeHistory")] ShapeHistory shapeHistory)
            : base(currentCanvas, strokeColor, fillColor, strokeThikness, isDashed, shapeHistory)
        {
            ControlName = "Circle";
        }

        /// <summary>
        /// Обработчик события нажатия левой кнопки мыши
        /// </summary>
        public override void MouseLeftButtonDown()
        {
            //Создание и добавление экземпляра круга на канвас, захват мыши
            CurrentCanvas.CaptureMouse();
            _startPoint = Mouse.GetPosition(CurrentCanvas);
            _shadowCircle = new Ellipse()
            {
                Stroke = new SolidColorBrush(StrokeColor.Invoke()),
                Fill = new SolidColorBrush(FillColor.Invoke()),
                StrokeThickness = StrokeThikness.Invoke()
            };

            if (IsDashed.Invoke())
            {
                _shadowCircle.StrokeDashArray = new DoubleCollection(new double[] { StrokeDashSize });
            }

            CurrentCanvas.Children.Add(_shadowCircle);
        }

        /// <summary>
        /// Обработчик события передвижения мыши
        /// </summary>
        public override void MouseMove()
        {
            // Изменение радиуса круга
            if (Mouse.LeftButton == MouseButtonState.Pressed && _shadowCircle != null)
            {
                Point endPoint = Mouse.GetPosition(CurrentCanvas);
                double deltaX = Math.Abs(_startPoint.X - endPoint.X);
                double deltaY = Math.Abs(_startPoint.Y - endPoint.Y);

                _shadowCircle.Height = Math.Max(deltaY * 2, deltaX * 2);
                _shadowCircle.Width = Math.Max(deltaY * 2, deltaX * 2);

                Canvas.SetTop(_shadowCircle, _startPoint.Y - deltaY);
                Canvas.SetLeft(_shadowCircle, _startPoint.X - deltaX);
            }
        }

        /// <summary>
        /// Обработчик события отжатия левой кнопки мыши
        /// </summary>
        public override void MouseLeftButtonUp()
        {

            _circle = _shadowCircle;
            CurrentCanvas.Children.Remove(_shadowCircle);
            _shadowCircle = null;

            var command = new AddShapeCommand(_circle);
            ShapeHistory.Add(command);
            CurrentCanvas.ReleaseMouseCapture();
        }
    }
}
