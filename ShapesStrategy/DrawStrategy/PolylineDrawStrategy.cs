﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Paint.Library;
using Paint.Library.Commands;
using Color = System.Windows.Media.Color;

namespace ShapesStrategy.DrawStrategy
{
    /// <summary>
    /// Семейство алгоритмов для построения(рисования) полилинии
    /// </summary>
    [Export(typeof(Paint.Library.DrawStrategy))]
    public class PolylineDrawStrategy : Paint.Library.DrawStrategy
    {
        private Polyline _polyline;

        [ImportingConstructor]
        public PolylineDrawStrategy(
            [Import("currentCanvas")] Canvas currentCanvas,
            [Import("strokeColor")] Func<Color> strokeColor,
            [Import("fillColor")] Func<Color> fillColor,
            [Import("strokeThikness")] Func<int> strokeThikness,
            [Import("isDashed")] Func<bool> isDashed,
            [Import("shapeHistory")] ShapeHistory shapeHistory)
            : base(currentCanvas, strokeColor, fillColor, strokeThikness, isDashed, shapeHistory)
        {
            ControlName = "Polyline";
        }

        /// <summary>
        /// Обработчик события отжатия левой кнопки мыши
        /// </summary>
        public override void MouseLeftButtonUp()
        {
            if (_polyline == null)
            {
                _polyline = new Polyline()
                {
                    Stroke = new SolidColorBrush(StrokeColor.Invoke()),
                    StrokeThickness = StrokeThikness.Invoke(),
                    Points = new PointCollection()
                };

                if (IsDashed.Invoke())
                {
                    _polyline.StrokeDashArray = new DoubleCollection(new double[] { StrokeDashSize });
                }
            }

            double x = Mouse.GetPosition(CurrentCanvas).X;
            double y = Mouse.GetPosition(CurrentCanvas).Y;

            _polyline.Points.Add(new System.Windows.Point(Convert.ToInt32(x), Convert.ToInt32(y)));
        }

        /// <summary>
        /// Обработчик события отжатия правой кнопки мыши
        /// </summary>
        public override void MouseRightButtonUp()
        {
            if (_polyline == null) return;
            var command = new AddShapeCommand(_polyline);
            ShapeHistory.Add(command);

            Canvas.SetTop(_polyline, 0);
            Canvas.SetLeft(_polyline, 0);
            _polyline = null;
        }
    }
}
