﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;

namespace Paint.Library
{    
	public class ShapeHistory
	{
        private List<ICommand> _drawCommands;

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public ICommand[] DrawCommands
        {
            get { return _drawCommands.ToArray(); }
            set { _drawCommands = value.ToList(); }
        }

        public int CurrentIndex { get; set; }

        public ShapeHistory() { }

	    public ShapeHistory(Canvas canvas)
	    {
            _drawCommands = new List<ICommand>();
            Canvas = canvas;
	    }

	    public void Add(ICommand command)
		{
            CutOffHistory();
			command.Execute(Canvas);
			_drawCommands.Add(command);
			CurrentIndex += 1;
		}

		public void Undo()
		{
			if (_drawCommands!= null && _drawCommands.Count != 0 && CurrentIndex > 0)
			{				
				_drawCommands[CurrentIndex - 1].Cancel(Canvas);
                Canvas.UpdateLayout();
                CurrentIndex -= 1;
            }
		}

		public void Redo()
		{
		    if (_drawCommands != null && _drawCommands.Count != 0 && CurrentIndex < _drawCommands.Count)
		    {
                _drawCommands[CurrentIndex].Execute(Canvas);
                CurrentIndex += 1;
            }		
        }

	    private void CutOffHistory()
	    {
	        int count = _drawCommands.Count;
            _drawCommands.RemoveRange(CurrentIndex, count - CurrentIndex);
	    }

	    public void ClearHitsory()
	    {
	        if (_drawCommands != null)
	        {
                _drawCommands.Clear();
                Canvas.Children.Clear();
            }	        
	    }

	    public void LoadHistory()
	    {
	        if (_drawCommands != null)
	        {
                foreach (var drawCommand in _drawCommands)
                {
                    drawCommand.Execute(Canvas);
                }
            }        
	    }

	    public Canvas Canvas { get; set; }
	}
}
