﻿using System;
using System.Windows.Controls;
using System.Windows.Media;

namespace Paint.Library
{
    /// <summary>
    /// Абстрактный класс событий мыши
    /// </summary>
    public abstract class DrawStrategy 
    {
        protected readonly int StrokeDashSize = 2; // Размер пунктира границы
        protected Canvas CurrentCanvas;
        protected Func<Color> StrokeColor;
        protected Func<Color> FillColor;
        protected Func<int> StrokeThikness;
        protected Func<bool> IsDashed;
	    public ShapeHistory ShapeHistory { get; set; }
        public string ControlName {get; set; }

        protected DrawStrategy(Canvas currentCanvas, Func<Color> strokeColor, Func<Color> fillColor, 
            Func<int> strokeThikness, Func<bool> isDashed, ShapeHistory shapeHistory)
        {
            CurrentCanvas = currentCanvas;
            StrokeColor = strokeColor;
            FillColor = fillColor;
            StrokeThikness = strokeThikness;
            IsDashed = isDashed;
	        ShapeHistory = shapeHistory;
        }

        /// <summary>
        /// Обработчик события нажатия левой кнопки мыши
        /// </summary>
        public virtual void MouseLeftButtonDown() { }

        /// <summary>
        ///  Обработчик события отжатия левой кнопки мыши
        /// </summary>
        public virtual void MouseLeftButtonUp() { }

        /// <summary>
        /// Обработчик события нажатия правой кнопки мыши
        /// </summary>
        public virtual void MouseRightButtonDown() { }

        /// <summary>
        /// Обработчик события отжатия правой кнопки мыши
        /// </summary>
        public virtual void MouseRightButtonUp() { }

        /// <summary>
        /// Обработчик события передвижения мыши
        /// </summary>
        public virtual void MouseMove() { }

        public virtual void Clear() { }

        public virtual void Initialize() { }

        public virtual void SetStrokeColor() { }

        public virtual void SetFillColor() { }

        public virtual void SetDashed() { }

        public virtual void SetStrokeThikness() { }
    }
}
