﻿using System;
using System.Windows.Shapes;

namespace Paint.Library
{
    public interface IEditDecorator
    {
        void SetEditAnchors(Shape shape);
        void RemoveEditAnchors(Shape shape);
        Type ShapeType();
    }
}
