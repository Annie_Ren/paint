﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Paint.Library
{
    /// <summary>
    /// Представляет слой для Shape на котором рисуются узловые точки фигур
    /// </summary>
    public class AnchorAdorner: Adorner
    {
        private VisualCollection visualChildren;
        protected override int VisualChildrenCount { get { return visualChildren.Count; } }
        protected override Visual GetVisualChild(int index) { return visualChildren[index]; }
        private Dictionary<Thumb, Point> _thumbs = new Dictionary<Thumb, Point>();

        public AnchorAdorner(Shape shape, Dictionary<Point, Action<object, DragCompletedEventArgs>> thumbPositionAndDragAction) :
            base(shape)
        {
            visualChildren = new VisualCollection(shape);
            
            foreach (var thumbOption in thumbPositionAndDragAction)
            {
                var thumb = GetAnchorThumb();
                thumb.Arrange(new Rect(thumbOption.Key, new Size(10, 10)));
                thumb.DragCompleted += new DragCompletedEventHandler(thumbOption.Value);
                thumb.DragCompleted += DragDeltaThumb;
                visualChildren.Add(thumb);
                _thumbs.Add(thumb, thumbOption.Key);
            }        
        }

        private void DragDeltaThumb(object sender, DragCompletedEventArgs e)
        {
            var thumb = sender as Thumb;
            var coord = _thumbs[thumb];

            var newCoord = new Point(coord.X + e.HorizontalChange, coord.Y + e.VerticalChange);
            thumb.Arrange(new Rect(newCoord, new Size(10,10)));

            _thumbs[thumb] = newCoord;

        }

        private Thumb GetAnchorThumb()
        {
            var thumb = new Thumb
            {
                Cursor = Cursors.SizeNESW,
                Height = 10,
                Width = 10,
                Opacity = 0.40,
                Background = new SolidColorBrush(Colors.MediumBlue)
            };
            return thumb;
        }
    }
}
