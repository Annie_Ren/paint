﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Paint.Library.Commands
{
    public class SetDashedCommand : ICommand
    {
        public bool IsDashed { get; set; }
        public int StrokeDashSize { get; set; }
        private Dictionary<Shape, bool> _shapesWithOldIsDashed = new Dictionary<Shape, bool>();
        private Dictionary<int, bool> _shapeIndexesWithOldIsDashed = new Dictionary<int, bool>();

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public Dictionary<int, bool> ShapeIndexesWithOldIsDashed
        {
            get { return _shapeIndexesWithOldIsDashed; }
            set { _shapeIndexesWithOldIsDashed = value; }
        }

        public SetDashedCommand() { }

        public SetDashedCommand(List<Shape> shapes, bool isDashed, int strokeDashSize)
        {
            IsDashed = isDashed;
            StrokeDashSize = strokeDashSize;

            var doubleCollectionOfDashSize = new DoubleCollection(new double[] {StrokeDashSize});

            foreach (var shape in shapes)
            {
                if (shape.StrokeDashArray.SequenceEqual(doubleCollectionOfDashSize))
                {
                    _shapesWithOldIsDashed.Add(shape, true);
                }
                else
                {
                    _shapesWithOldIsDashed.Add(shape, false);
                }
            }
        }

        public void Execute(Canvas canvas)
        {
            if (_shapesWithOldIsDashed?.Count > 0)
            {
                foreach (var shapeWithOldIsDashed in _shapesWithOldIsDashed)
                {
                    if (IsDashed)
                    {
                        shapeWithOldIsDashed.Key.StrokeDashArray = new DoubleCollection(new double[] { StrokeDashSize });
                        var shapeIndex = canvas.Children.IndexOf(shapeWithOldIsDashed.Key);
                        if (!_shapeIndexesWithOldIsDashed.ContainsKey(shapeIndex))
                        {
                            _shapeIndexesWithOldIsDashed.Add(shapeIndex, shapeWithOldIsDashed.Value);
                        }
                    }
                    else
                    {
                        shapeWithOldIsDashed.Key.StrokeDashArray.Clear();
                        var shapeIndex = canvas.Children.IndexOf(shapeWithOldIsDashed.Key);
                        if (!_shapeIndexesWithOldIsDashed.ContainsKey(shapeIndex))
                        {
                            _shapeIndexesWithOldIsDashed.Add(shapeIndex, shapeWithOldIsDashed.Value);
                        }
                    }
                }
            }           
        }

        public void Cancel(Canvas canvas)
        {
            if (_shapesWithOldIsDashed?.Count > 0)
            {
                foreach (var shapeWithOldIsDashed in _shapesWithOldIsDashed)
                {
                    if (shapeWithOldIsDashed.Value)
                    {
                        shapeWithOldIsDashed.Key.StrokeDashArray = new DoubleCollection(new double[] { StrokeDashSize });
                    }
                    else
                    {
                        shapeWithOldIsDashed.Key.StrokeDashArray.Clear();
                    }
                }
            }
            else
            {
                _shapesWithOldIsDashed = new Dictionary<Shape, bool>();

                foreach (var shapeIndexWithOldIsDashed in _shapeIndexesWithOldIsDashed)
                {
                    if (shapeIndexWithOldIsDashed.Value)
                    {
                        var shape = (Shape) canvas.Children[shapeIndexWithOldIsDashed.Key];

                        shape.StrokeDashArray = new DoubleCollection(new double[] { StrokeDashSize });
                        _shapesWithOldIsDashed.Add(shape, shapeIndexWithOldIsDashed.Value);                        
                    }
                    else
                    {
                        var shape = (Shape)canvas.Children[shapeIndexWithOldIsDashed.Key];

                        shape.StrokeDashArray.Clear();
                        _shapesWithOldIsDashed.Add(shape, shapeIndexWithOldIsDashed.Value);
                    }
                }
            }
        }
    }
}