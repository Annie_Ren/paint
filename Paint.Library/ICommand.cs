﻿using System.Windows.Controls;

namespace Paint.Library
{
    public interface ICommand
    {
        void Execute(Canvas canvas);
        void Cancel(Canvas canvas);
    }
}
